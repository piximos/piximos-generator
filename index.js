#!/usr/bin/env node

const inquirer = require('inquirer');
const fs = require('fs');
const rimraf = require('rimraf');
// KEYS
const KEY_PROJECT_NAME = 'project-name';
const KEY_PACKAGE_NAME = 'package-name';
const KEY_DESCRIPTION = 'package-description';
const KEY_AUTHOR = 'package-author';
const KEY_PROJECT_VERSION = 'project-version';
const KEY_TEST_DIR = 'unit-tests-directory';
const KEY_TEST_EXT = 'unit-tests-extension';
const KEY_GIT_HOOKS = 'git-hooks';
const KEY_DEPLOY_LIB = 'lib-deployment';
const KEY_CONFIRM = 'confirm';

// Global variables
const CURR_DIR = process.cwd();
const TEMPLATE_FOLDER = `${__dirname}/template`;
let NEW_PROJECT_PATH = '';

// QUESTIONS
const QUESTIONS = [
    {
        name: KEY_PROJECT_NAME,
        type: 'input',
        message: 'What is the name of your project',
        validate: (name) => {
            if (/^(\w+\.?)*(\w\s?)+$/.test(name)) {
                return true;
            }
            return 'Please enter a valid project name.';
        }
    },
    {
        name: KEY_PACKAGE_NAME,
        type: 'input',
        message: 'What is the name of your package',
        validate: (name) => {
            if (/(^[a-z][a-z0-9]+(\-[a-z0-9]+)*$)|(^@[a-z][a-z0-9]+\/[a-z][a-z0-9]+(\-[a-z0-9]+)*$)/.test(name)) {
                return true;
            }
            return 'Please enter a valid package name.';
        }
    },
    {
        name: KEY_PROJECT_VERSION,
        type: 'input',
        message: 'What is the version of your project',
        validate: (version) => {
            if (/^([0-9]|[1-9][0-9])\.([0-9]|[1-9][0-9])\.([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$/.test(version)) {
                return true;
            }
            return 'Please enter a valid project version.';
        }
    },
    {
        name: KEY_DESCRIPTION,
        type: 'input',
        message: 'Describe your project',
    },
    {
        name: KEY_AUTHOR,
        type: 'input',
        message: 'Author info',
    },
    {
        name: KEY_TEST_DIR,
        type: 'list',
        message: 'Please select the unit tests directory name.',
        choices: [
            'spec',
            'test'
        ],
        default: 1
    },
    {
        name: KEY_TEST_EXT,
        type: 'list',
        message: 'Please select the unit tests files format.',
        choices: [
            '*.spec.ts',
            '*.test.ts'
        ],
        default: 1
    },
    {
        name: KEY_GIT_HOOKS,
        type: 'confirm',
        message: 'Do you want to add git hooks for file formatting and branch name validation pre-commit? (yes)',
        default: true
    },
    {
        name: KEY_DEPLOY_LIB,
        type: 'confirm',
        message: 'Will you deploy this project as a library? (no)',
        default: false
    }
];

// Methods
const askQuestions = () => {
    return inquirer.prompt(QUESTIONS)
        .then(answers => {
            console.log('\n=========================================================\n');
            console.log('\tPlease check your answers');
            console.log('\n=========================================================\n');
            console.log('\t\tProject name : ', answers[KEY_PROJECT_NAME]);
            console.log('\t\tPackage name : ', answers[KEY_PACKAGE_NAME]);
            console.log('\t\tPackage description : ', answers[KEY_DESCRIPTION]);
            console.log('\t\tPackage author : ', answers[KEY_AUTHOR]);
            console.log('\t\tProject version : ', answers[KEY_PROJECT_VERSION]);
            console.log('\t\tTests directory : ', answers[KEY_TEST_DIR]);
            console.log('\t\tTests extension : ', answers[KEY_TEST_EXT]);
            console.log('\t\tAdd git hooks : ', answers[KEY_GIT_HOOKS]);
            console.log('\t\tDeploy as a library : ', answers[KEY_DEPLOY_LIB]);
            console.log('\n=========================================================\n');

            return inquirer.prompt([{
                name: KEY_CONFIRM,
                type: 'confirm',
                message: 'Are the values okay? (no)',
                default: false
            }]).then(answers2 => {
                if (answers2[KEY_CONFIRM] === true) {
                    return answers;
                } else {
                    return askQuestions();
                }
            });

        });
};

createContent = (answers, templatePath, newProjectPath) => {
    const filesToCreate = fs.readdirSync(templatePath);

    filesToCreate.forEach(file => {

        const origFilePath = `${templatePath}/${file}`;
        console.log('Copying : ', origFilePath);

        // get stats about the current file
        const stats = fs.statSync(origFilePath);

        if (stats.isFile()) {
            const contents = fs.readFileSync(origFilePath, 'utf8');

            const writePath = `${newProjectPath}/${file}`;
            console.log(`Creating : ${newProjectPath}/${file}`);
            fs.writeFileSync(writePath, contents, 'utf8');
            console.log(`Created : ${newProjectPath}/${file} \n`);
        } else if (stats.isDirectory()) {
            fs.mkdirSync(`${newProjectPath}/${file}`);

            // recursive call
            createContent(answers, `${templatePath}/${file}`, `${newProjectPath}/${file}`);
        }
    });
};

createTestPath = (answers, projectPath) => {
    fs.mkdirSync(`${projectPath}/${answers[KEY_TEST_DIR]}`);
    const testFileName = answers[KEY_TEST_EXT] === '*.spec.ts' ? 'index.spec.ts' : 'index.test.ts';
    fs.writeFileSync(`${projectPath}/${answers[KEY_TEST_DIR]}/${testFileName}`, '', 'utf8');
};

alterPackageJson = (answers) => {
    const packageJSONPath = `${NEW_PROJECT_PATH}/package.json`;
    const packageFile = fs.readFileSync(packageJSONPath, 'utf8');
    let packageJSON = JSON.parse(packageFile);
    packageJSON.name = answers[KEY_PACKAGE_NAME];
    packageJSON.version = answers[KEY_PROJECT_VERSION];
    packageJSON.scripts.test = `npm run lint && mocha --require ts-node/register --watch-extensions ts,tsx **/${answers[KEY_TEST_DIR]}/**/${answers[KEY_TEST_EXT]}`
    packageJSON.author = answers[KEY_AUTHOR];
    packageJSON.description = answers[KEY_DESCRIPTION];

    if (!answers[KEY_GIT_HOOKS]) {
        delete packageJSON.husky;
        rimraf.sync(`${NEW_PROJECT_PATH}/tools`);
    }

    // console.log(packageJSON);
    console.log('Updated : ', packageJSONPath);
    fs.writeFileSync(packageJSONPath, JSON.stringify(packageJSON));

};

alterTSConfig = (answers) => {
    const tsConfigPath = `${NEW_PROJECT_PATH}/tsconfig.json`;
    const tsConfigFile = fs.readFileSync(tsConfigPath, 'utf8');
    let tsConfig = JSON.parse(tsConfigFile);

    tsConfig['compilerOptions'].declaration = answers[KEY_DEPLOY_LIB];
    tsConfig['exclude'][1] = `**/${answers[KEY_TEST_DIR]}/**`;


    console.log(tsConfig);
    console.log('Updated : ', tsConfigPath);
    fs.writeFileSync(tsConfigPath, JSON.stringify(tsConfig));

};

// Execute
askQuestions()
    .then(finalAnswers => {
        console.log('Creating project directory.');
        NEW_PROJECT_PATH = `${CURR_DIR}/${finalAnswers[KEY_PROJECT_NAME]}`;
        fs.mkdirSync(NEW_PROJECT_PATH);
        console.log('Template path : ', TEMPLATE_FOLDER);
        console.log('Project dir : ', NEW_PROJECT_PATH);
        createContent(finalAnswers, TEMPLATE_FOLDER, NEW_PROJECT_PATH);
        createTestPath(finalAnswers, NEW_PROJECT_PATH);
        alterPackageJson(finalAnswers);
        alterTSConfig(finalAnswers);
    });

