# Piximos project generator
A simple TypeScript Node.js project generator. The generated project has the the following structure :
```
project-name 
    |_src
        |_index.ts
    |_test
    |_.gitignore
    |_.prettierrc
    |_jestconfig.json
    |_package.json
    |_README.md
    |_tsconfig.json
    |_tslint.json
```

By default, in version `1.0.0` the unit tests library is Mocha. In future releases, more unit tests libraries will be added as well as further templates.

## Installation

To install the project generator, use global installation. Simply type `npm i -g @piximos/generator`.

## Usage

To generate a project, type in the following command in your CLI `pixi-generate`. 
You will be prompted to :

 - Input the project folder name
 - Input the package name
 - Input the package version
 - Input a description for your package (optional)
 - Input the author of your package
 - Select the test directory
 - Select the test files format
 - Select whether or to add pre-commit git hooks for file formatting and branch names. (default : yes)
 - Select whether or not to add type declarations to the `tsconfig.json` of your newly created project.

After completing the the installation, type the following command : `cd <project-folder-name>; npm i`

## Git hooks
If git hooks are enabled, it uses the following pattern to validate branch names `TASK-* | vx.x.x | master`.
to change the pattern, go to `<project-folder-name>/tools/local/branch-name.sh` and change it.

It also pushes new version tags with every `vx.x.x` branch. To change this, go to `<project-folder-name>/tools/local/version-tags-push.sh`.
