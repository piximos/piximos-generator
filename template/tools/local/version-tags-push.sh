#!/usr/bin/env bash

branch_name=$(git symbolic-ref -q HEAD)
branch_name=${branch_name##refs/heads/}
branch_name=${branch_name:-HEAD}

echo "> The current branch is : ${branch_name}"
# check if the current branch is a one of new version push
if [[ ${branch_name} =~ ^v[0-9]{1,3}\.[0-9]{1,3}.[0-9]{1,3}$ ]] ; then
    echo "> BRANCH NAME :  '${branch_name}' is for new version releases."
else
    echo "> BRANCH NAME :  '${branch_name}' is not for new version releases."
    echo "> EXIT."
    exit 0
fi

# extract version number
versionNumber=${branch_name#?}
IFS='.' read -r -a versionNumber <<< "$versionNumber"
tag_to_add="v${versionNumber[0]}.${versionNumber[1]}.${versionNumber[2]}"

echo "> EXTRACTED VERSION FROM BRANCH NAME IS $tag_to_add"

# extracting tag
git tag -v ${tag_to_add}
tag_exists=$?
if [[ ${tag_exists} -ne 0 ]]; then
    echo "> TAG ${tag_to_add} DOESN'T EXIST. PROCEEDING."
else
    echo "> TAG ${tag_to_add} ALREADY EXIST. EXITING."
    exit 0
fi

# adding tag
echo "> ADD TAG ${tag_to_add}"
tag_message="Adding new release tag for version ${tag_to_add}"
git tag -a ${tag_to_add} -m "${tag_message}"
