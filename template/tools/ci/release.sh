#!/usr/bin/env bash

echo "> CHECKING BRANCH NAME $CI_COMMIT_REF_NAME"
branch_name=$CI_COMMIT_REF_NAME
npm config set git-tag-version=false

if [[ ${branch_name} =~ ^v[0-9]{1,3}\.[0-9]{1,3}.[0-9]{1,3}$ ]] ; then
    echo "> BRANCH NAME :  '{$branch_name}' is valid."
else
    echo "> BRANCH NAME '${branch_name}' is not a valid one. Please use a valid branch name."
    echo "> A valid branch name must be 'master', is 'vx.x.x' or starts with 'TASK-'"
    exit 1
fi

echo "> EXTRACTING VERSION FROM BRANCH NAME"
echo ${branch_name}
versionNumber=${branch_name#?}
IFS='.' read -r -a versionNumber <<< "$versionNumber"

npmVersion="${versionNumber[0]}.${versionNumber[1]}.${versionNumber[2]}"

echo "> EXTRACTED VERSION FROM BRANCH NAME IS $versionNumber"

echo "> BUILDING PROJECT"
npm run build

echo "> pushing version ${npmVersion}"
npm --no-git-tag-version version ${npmVersion}

echo "> SENDING AUTHENTICATION REQUEST THROUGH CURL"

echo "> SETTING THE TOKEN LOCALLY"
npm set //registry.npmjs.org/:_authToken=${NPM_AUTH_TOKEN}

echo "> THE CONTENTS OF THE LOCAL .NPMRC ARE : "
cat ~/.npmrc


echo "> PUBLISHING"
npm publish

exit 0;
