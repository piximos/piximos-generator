#!/usr/bin/env bash

#branch_name=$(git symbolic-ref -q HEAD)
#branch_name=${branch_name##refs/heads/}
#branch_name=${branch_name:-HEAD}

branch_name=$CI_COMMIT_REF_NAME
echo "The current branch is : ${branch_name}"

if [[ ${branch_name} =~ TASK-.*|master.*|^v[0-9]{1,3}\.[0-9]{1,3}.[0-9]{1,3}$|^v[0-9]{1,3}\.[0-9]{1,3}.[0-9]{1,3}..* ]] ; then
    echo "BRANCH NAME :  '{$branch_name}' is valid."
    exit 0
else
    echo "BRANCH NAME '${branch_name}' is not a valid one. Please use a valid branch name."
    echo "A valid branch name must be 'master', is 'vx.x.x' or starts with 'TASK-'"
    exit 1
fi
